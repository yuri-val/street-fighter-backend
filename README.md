# Backend for Street Fighter

## Frontend

### Repo
https://bitbucket.org/yuri-val/street-fighter/src/master/

### Demo
https://kind-goldstine-ce3d02.netlify.com/

## Backend

### Demo

You can find lastest `master`'s version of project at https://radiant-tundra-60765.herokuapp.com/

### How to run:

1. Clone this repo
2. Go to project folder
3. Install dependencies

`npm install`

4. Start project

`npm run start`

It starts project on http://localhost:3000

### How it work

You work with Express.js server. Using `sequelize` as ORM and sqlite as Database (in development).

For Heroku demo using PostgreSQL as Database.

Config you can find at `./config/config.josn`

### Endpoints

All endponts you can find when start project on root path `http://localhost:3000`

or go to `./routes/api_doc/index.js` 

