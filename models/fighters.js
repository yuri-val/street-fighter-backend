'use strict';
module.exports = (sequelize, DataTypes) => {
  var Fighter = sequelize.define('Fighter', {
    name: DataTypes.STRING,
    health: DataTypes.INTEGER, 
    attack: DataTypes.INTEGER, 
    defense: DataTypes.INTEGER,
    source: DataTypes.STRING
  });

  return Fighter;
};