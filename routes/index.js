var express = require('express');
var router = express.Router();

var apiDoc = require('./api_doc')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.send(apiDoc);
});

module.exports = router;
