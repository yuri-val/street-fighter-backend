var models  = require('../models');
var express = require('express');
var router = express.Router();

/* GET: /fighters  - listing. */
router.get('/', function(req, res, next) {
  models.Fighter.findAll().then(function(fighter) {
    res.send(fighter);
  });
});

/* GET: /fighters/:id */
router.get('/:id', function(req, res, next) {
  models.Fighter.findByPk(req.params['id']).then((fighter) => {
    if (fighter)
      res.send(fighter);
    else
      res.status(404).send()
  });
});

/* POST: /fighters */
router.post('/', function(req, res, next) {
  let data = req.body;
  if (data.constructor !== Array) data = [data];
  models.Fighter.bulkCreate(data)
      .then(fighters => res.send(fighters))
});

/* PUT: /fighters/:id */
router.put('/:id', function(req, res, next) {
  let data = req.body;
  models.Fighter.findByPk(req.params['id'])
    .then(fighter => { 
      if (fighter)
        fighter.update(data)
          .then(fighter => {res.send(fighter) }) 
      else
        res.status(404).send()
    })
});

/* DELETE: /fighters/:id */
router.delete('/:id', function(req, res, next) {
  models.Fighter.findByPk(req.params['id'])
    .then(fighter => { 
      if (fighter)
        fighter.destroy()
          .then(fighter => {res.send(fighter) }) 
      else
        res.status(404).send()
    })
});



module.exports = router;
