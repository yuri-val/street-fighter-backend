const apiDoc = {
  resources: {
    fighters: [
      {
        method: 'GET',
        endpoint: '/fighters',
        description: 'отримання масиву всіх бійців'
      },
      {
        method: 'GET',
        endpoint: '/fighters/:id',
        description: 'отримання одного бійця по ID'
      },
      {
        method: 'POST',
        endpoint: '/fighters',
        description: 'створення бійця/бійців за даними з тіла запиту'
      },
      {
        method: 'PUT',
        endpoint: '/fighters/:id',
        description: 'оновлення бійця за даними з тіла запиту'
      },
      {
        method: 'DELETE',
        endpoint: '/fighters/:id',
        description: 'видалення одного бійця по ID'
      }
      
    ]
  }
}

module.exports = apiDoc;